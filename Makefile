proj_dir := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

.DEFAULT_GOAL := run

build:
	podman build -t openmw-generic:0000 $(proj_dir)

run: build
	podman run --rm -v $(proj_dir)/out:/opt/openmwbuild openmw-generic:0000

bash sh: build
	podman run -it --rm -v $(proj_dir)/out:/opt/openmwbuild openmw-generic:0000 bash
